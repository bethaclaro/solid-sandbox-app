## Usage

### Installation

<br>

```bash
$ yarn install
```

<br>

### To run on localhost:3000:

```bash
$ yarn dev
```

or

```bash
$ yarn start
```

<br>

### Learn more on the [Solid Website](https://solidjs.com).

<br>
