import { css } from "@emotion/css";
import { RouteDefinition, useRoutes } from "@solidjs/router";
import { Component, createMemo, createSignal, useContext } from "solid-js";
import { Portal } from "solid-js/web";
import {
  appContainer,
  bodyContainer,
  sidebarContainer,
} from "./assets/common/styles";
import { registerHotkey } from "./assets/common/utils";
import Sidebar from "./assets/components/Sidebar";
import { AppContext } from "./assets/context";
import EmptyPage from "./assets/pages/EmptyPage";
import Notifications from "./assets/pages/Notifications";
import Overview from "./assets/pages/Overview";
import SpaceX from "./assets/pages/SpaceX";
import { TextField, Button } from "@suid/material";

const appRoutes: RouteDefinition[] = [
  {
    path: "/overview",
    component: Overview,
  },
  {
    path: "/notifications",
    component: Notifications,
  },
  {
    path: "/spacex",
    component: SpaceX,
  },
  {
    path: "/contact",
    component: EmptyPage,
  },
];

const overlay = css`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  display: none;
  background: RGB(0, 0, 0, 0.5);
  justify-content: center;
  z-index: 10;
`;

const App: Component = () => {
  const Routes = useRoutes(appRoutes);
  const { overlayVisible, _setOverlayVisible, focusOnSearchInput } =
    useContext(AppContext);

  const cancelModal = () => {
    _setOverlayVisible(false);
  };

  registerHotkey({ keybinding: "command+k", handlerFn: focusOnSearchInput });
  registerHotkey({ keybinding: "esc", handlerFn: cancelModal });

  const overlayStyle = createMemo(() =>
    overlayVisible()
      ? [
          overlay,
          css`
            display: flex;
          `,
        ]
      : [overlay]
  );

  return (
    <>
      <div class={appContainer}>
        <div class={sidebarContainer}>
          <Sidebar />
        </div>
        <div class={bodyContainer}>
          <Routes />
        </div>
        <Portal mount={document.getElementById("root") as Node}>
          <div class={css(overlayStyle())}>
            <div
              class={css`
                margin-top: 20vh;
                display: flex;
                gap: 8px;
                flex-direction: column;
                width: 50vw;
                align-items: center;
              `}
            >
              <TextField
                class={css`
                  background: white;
                  color: black;
                `}
                variant="filled"
                id="modal-search-box"
                placeholder="Start typing to search..."
                fullWidth
              />
              <Button
                class={css`
                  width: 100px;
                `}
                onclick={cancelModal}
                variant="contained"
              >
                dismiss
              </Button>
            </div>
          </div>
        </Portal>
      </div>
    </>
  );
};

export default App;
