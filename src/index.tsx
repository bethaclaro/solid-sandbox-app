/* @refresh reload */
import { render } from "solid-js/web";

import "./index.css";
import App from "./App";
import { Router } from "@solidjs/router";
import { QueryClientProvider, QueryClient } from "@tanstack/solid-query";
import { AppContextProvider } from "./assets/context";

const root = document.getElementById("root");

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
  throw new Error(
    "Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got mispelled?"
  );
}

const queryClient = new QueryClient();

render(
  () => (
    <QueryClientProvider client={queryClient}>
      <Router>
        <AppContextProvider>
          <App />
        </AppContextProvider>
      </Router>
    </QueryClientProvider>
  ),
  root!
);
