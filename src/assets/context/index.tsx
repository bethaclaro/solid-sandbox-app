import { createContext, createSignal, JSX } from "solid-js";
import { createStore } from "solid-js/store";

const [overlayVisible, _setOverlayVisible] = createSignal<boolean>(false);
const focusOnSearchInput = () => {
  _setOverlayVisible(true);
  const inputElement = document.getElementById("modal-search-box");
  inputElement?.focus();
};

const initialValue = {
  overlayVisible,
  _setOverlayVisible,
  focusOnSearchInput,
};

export const AppContext = createContext(initialValue);

export const AppContextProvider = ({
  children,
}: {
  children: Element | JSX.Element;
}) => {
  return (
    <AppContext.Provider
      value={{ overlayVisible, _setOverlayVisible, focusOnSearchInput }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppContext;
