import { css } from "@emotion/css";
import { For, useContext } from "solid-js";
import { ColumnFlexBox } from "../common/styles";
import { SidebarGroup } from "../common/types";
import AppContext from "../context";
import {
  TextField,
  List,
  ListItemButton,
  ListSubheader,
  ListItemText,
  ListItemIcon,
} from "@suid/material";
import SendIcon from "@suid/icons-material/Send";
import WindowIcon from "@suid/icons-material/Window";
import NotifIcon from "@suid/icons-material/Notifications";
import MovingIcon from "@suid/icons-material/Moving";
import RocketIcon from "@suid/icons-material/RocketLaunch";
import { useNavigate } from "@solidjs/router";

const sidebarItems: SidebarGroup[] = [
  {
    label: "General",
    items: [
      {
        label: "Overview",
        path: "/overview",
        icon: SendIcon,
      },
      {
        label: "Notifications",
        path: "/notifications",
        icon: NotifIcon,
      },
    ],
  },
  {
    label: "Space Stuff",
    items: [
      {
        label: "SpaceX",
        path: "/spacex",
        icon: RocketIcon,
      },
      {
        label: "Another menu",
        path: "/contact",
        icon: MovingIcon,
      },
    ],
  },
];

const Sidebar = () => {
  const { focusOnSearchInput } = useContext(AppContext);
  const navigate = useNavigate();
  return (
    <div
      class={css([
        ColumnFlexBox,
        css`
          padding: 20px;
          gap: 10px;
        `,
      ])}
    >
      <h3>Horizon</h3>
      <div>
        <TextField
          id="search-input"
          placeholder="Quick find..."
          onclick={focusOnSearchInput}
          class={css`
            background: white;
            color: black;
          `}
          variant="outlined"
        />
      </div>
      <div class={ColumnFlexBox}>
        <For each={sidebarItems}>
          {(group) => {
            return (
              <List
                component="nav"
                subheader={
                  <ListSubheader component="div">{group.label}</ListSubheader>
                }
              >
                <For each={group.items}>
                  {(item) => {
                    return (
                      <ListItemButton onClick={() => navigate(item.path)}>
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText primary={item.label} />
                      </ListItemButton>
                    );
                  }}
                </For>
              </List>
            );
          }}
        </For>
      </div>
    </div>
  );
};

export default Sidebar;
