import { css } from "@emotion/css";
import { createSignal, For, Show } from "solid-js";
import { Dynamic } from "solid-js/web";
import {
  ButtonStyleOverride,
  ColumnFlexBox,
  FlexBox,
  HeaderWrapper,
} from "../../common/styles";
import EmptyPage from "../EmptyPage";
import Rockets from "./Rockets";
import { Button, Typography } from "@suid/material";

const tabs = [
  {
    tabName: "rockets",
    title: "Rockets",
    component: Rockets,
  },
  {
    tabName: "launchpads",
    title: "Launch Pads",
    component: EmptyPage,
  },
];

const SpaceX = () => {
  const [selectedTab, _setSelectedTab] = createSignal<string>("rockets");

  return (
    <div class={ColumnFlexBox}>
      <div class={HeaderWrapper}>
        <Typography variant="h4">SpaceX</Typography>
      </div>
      <div
        class={css([
          FlexBox,
          css`
            gap: 1px;
            margin: 20px 30px;
          `,
        ])}
      >
        <For each={tabs} fallback={<div>Loading...</div>}>
          {(item) => {
            return (
              <Button
                class={ButtonStyleOverride}
                variant="text"
                onClick={() => _setSelectedTab(item.tabName)}
              >
                {item.title}
              </Button>
            );
          }}
        </For>
      </div>
      <hr
        class={css`
          border: 1px solid #ebebeb;
          margin: 0px 30px;
        `}
      />
      <For each={tabs} fallback={<div>Loading...</div>}>
        {(item) => (
          <Show when={selectedTab() === item.tabName}>
            <Dynamic component={item.component} />
          </Show>
        )}
      </For>
    </div>
  );
};

export default SpaceX;
