import { css } from "@emotion/css";
import { ColumnFlexBox, FlexBox } from "../../common/styles";
import { createQuery, CreateQueryResult } from "@tanstack/solid-query";
import { gql, request } from "graphql-request";
import { createMemo, createSignal, For, Show } from "solid-js";
import {
  TextField,
  TableContainer,
  TableHead,
  Table,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@suid/material";
import { SelectChangeEvent } from "@suid/material/Select";

const graphqlEndpoint = "https://spacex-production.up.railway.app/";

const rocketQuery = gql`
  query getRockets {
    rockets {
      name
      description
      country
    }
  }
`;

type RocketData = {
  name: string;
  description: string;
  country: string;
};
type gqlResult = {
  rockets: RocketData[];
};

const tableheaders = ["Name", "Description", "Country"];

const Rockets = () => {
  const [countries, _setCountries] = createSignal<string[]>([]);
  const [searchTxt, _setSearchTxt] = createSignal<string>("");
  const [selectedCountry, _setSelectedCountry] = createSignal<string>("");

  const rocketDataQuery: CreateQueryResult<gqlResult> = createQuery(
    () => ["rockets"],
    async () => request(graphqlEndpoint, rocketQuery),
    {
      onSuccess: (res) => {
        const temp = res.rockets.map((item) => item.country);
        const newarr: string[] = Array.from(new Set(temp));
        _setCountries(newarr);
      },
    }
  );

  const filteredResultset = createMemo(() => {
    // filter by name
    const temp = rocketDataQuery.data?.rockets.filter((item) =>
      item.name.includes(searchTxt())
    );

    //filter by country
    let newArr = temp;
    if (selectedCountry()) {
      newArr = temp?.filter((item) => item.country === selectedCountry());
    }

    return newArr;
  });

  return (
    <div class={ColumnFlexBox}>
      <div
        class={css([
          FlexBox,
          css`
            justify-content: space-between;
            box-sizing: border-box;
            margin-bottom: 30px;
            alignt-items: center;
            padding: 20px 40px;
          `,
        ])}
      >
        <div
          class={css([
            FlexBox,
            css`
              gap: 12px;
              align-items: center;
            `,
          ])}
        >
          <span>Country:</span>
          <select
            class={css`
              height: 56px;
            `}
            name="selected-country"
            id="selected-country"
            onchange={(props) => {
              const target = props.target as HTMLSelectElement;
              _setSelectedCountry(target.value);
            }}
          >
            <option value="">All..</option>
            <For
              each={countries()}
              fallback={<option>Loading options...</option>}
            >
              {(item) => <option value={item}>{item}</option>}
            </For>
          </select>
        </div>
        <div>
          <span>
            <TextField
              id="rocket-search-txt"
              placeholder="Search..."
              onChange={(inputProps) => {
                const target = inputProps.target as HTMLInputElement;
                _setSearchTxt(target.value);
              }}
            />
          </span>
        </div>
      </div>
      <Show when={filteredResultset()} fallback={<div>Loading...</div>} keyed>
        <div
          class={css`
            padding: 20px 40px;
          `}
        >
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <For each={tableheaders}>
                    {(item) => <TableCell>{item}</TableCell>}
                  </For>
                </TableRow>
              </TableHead>
              <TableBody>
                <For each={filteredResultset()}>
                  {(item) => (
                    <TableRow>
                      <TableCell>{item.name}</TableCell>
                      <TableCell>{item.description}</TableCell>
                      <TableCell>{item.country}</TableCell>
                    </TableRow>
                  )}
                </For>
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </Show>
      <Show when={filteredResultset()?.length === 0}>
        <div>No records found...</div>
      </Show>
    </div>
  );
};

export default Rockets;
