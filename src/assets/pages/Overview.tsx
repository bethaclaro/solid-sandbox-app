import { css } from "@emotion/css";
import { ColumnFlexBox, HeaderWrapper } from "../common/styles";
import { Typography } from "@suid/material";

const Overview = () => {
  return (
    <div class={ColumnFlexBox}>
      <div class={HeaderWrapper}>
        <Typography variant="h4">Overview</Typography>
      </div>
      <hr
        class={css`
          border: 1px solid #ebebeb;
          margin: 0px 30px;
        `}
      />
    </div>
  );
};

export default Overview;
