import { css } from "@emotion/css";
import { ColumnFlexBox, HeaderWrapper } from "../common/styles";
import { Typography } from "@suid/material";

const EmptyPage = () => {
  return (
    <div class={ColumnFlexBox}>
      <div class={HeaderWrapper}>
        <Typography variant="h4">Page under construction</Typography>
      </div>
      <hr
        class={css`
          border: 1px solid #ebebeb;
          margin: 0px 30px;
        `}
      />
    </div>
  );
};

export default EmptyPage;
