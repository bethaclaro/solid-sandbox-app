import { css } from "@emotion/css";

export const appContainer = css`
  width: 100vw;
  height: 100vh;
  display: flex;
`;

export const sidebarContainer = css`
  width: 15%;
  background: #f9f9f9;
  padding: 10px;
`;

export const bodyContainer = css`
  flex: 1;
  padding: 10px;
`;

export const ColumnFlexBox = css`
  display: flex;
  flex-direction: column;
`;

export const FlexBox = css`
  display: flex;
`;

export const ButtonStyleOverride = css`
  border-radius: unset !important;
`;

export const ButtonTabHighlight = css`
  border-bottom: 1px solid #1976d2 !important;
`;

export const HeaderWrapper = css`
  padding: 30px;
`;
