import hotkeys from "hotkeys-js";
import { KeyboardShortcutDefinition } from "./types";

export const registerHotkey = (newHotkey: KeyboardShortcutDefinition) => {
  hotkeys(newHotkey.keybinding, newHotkey.handlerFn);
};
