export type SideBarItem = {
  label: string;
  path: string;
  icon?: any;
};

export type SidebarGroup = {
  label: string;
  items: SideBarItem[];
};

export type KeyboardShortcutDefinition = {
  keybinding: string;
  handlerFn: () => void;
};

export type KeyboardShortcutList = {
  [key: string]: KeyboardShortcutDefinition;
};
